<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    private function portfolio($limit = 6){
        return $this->getDoctrine()->getManager()
            ->getRepository('AppBundle:Portfolio')
            ->findBy(
                array(),
                array('id' => 'DESC')
            );
    }
    private function categorys_portfolio(){
        return $this->getDoctrine()->getManager()->getRepository('AppBundle:Category')->show_project();
    }
    private function portfolio_item($id = 1){
        $port_ob = $this->getDoctrine()->getManager()
            ->getRepository('AppBundle:Portfolio')
            ->findById($id);
        $port = $port_ob[0];

        return $port;
    }
    private function portfolio_last(){
        $port_ob = $this->getDoctrine()->getManager()
            ->getRepository('AppBundle:Portfolio')
            ->findBy(
                array(),
                array('id' => 'DESC')
            );
        if(isset($port_ob[0]))
        $port = $port_ob[0];
        else
            $port = null;
        return $port;
    }
    private function getTags($json = false){
        $tags = $this->getDoctrine()->getManager()
            ->getRepository('AppBundle:Tag')
            ->findBy(
                array(),
                array('name' => 'DESC')
            );
        if($json){
//            $array = array();
//            foreach($tags as $tag){
//                $array[$tag->getId()] = $tag->getTag();
//            }
//            $array = json_encode($array);
//            $tags = $array;
        }

        return $tags;
    }
    private function base(){
        $base['tags'] = $this->getTags();
        $base['p_l'] = $this-> portfolio_last();
        return $base;
    }
    /**
     * @Route("/{_locale}", name="index",defaults={"_locale" = "en"}, requirements={"_locale": "en|es"})
     *
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
            'actual' => '',
            'portfolio' => $this->portfolio(),
            'base' => $this->base(),
        ));
    }
    /**
     * @Route("/{_locale}/service", name="service" ,defaults={"_locale" = "en"}, requirements={"_locale": "en|es"})
     * @Route("/service" ,defaults={"_locale" = "en"})
     *
     */
    public function serviceAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('service/index.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
            'actual' => '/service',
            'base' => $this->base(),
        ));
    }
    /**
     * @Route("/{_locale}/about", name="about" ,defaults={"_locale" = "en"}, requirements={"_locale": "en|es"})
     * @Route("/about" ,defaults={"_locale" = "en"})
     */
    public function aboutAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('about/index.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
            'actual' => '/about',
            'portfolio' => $this->portfolio(),
            'base' => $this->base(),
        ));
    }
    /**
     * @Route("/{_locale}/contact", name="contact" ,defaults={"_locale" = "en"}, requirements={"_locale": "en|es"})
     * @Route("/contact" ,defaults={"_locale" = "en"})
     */
    public function contactAction(Request $request)
    {
        $info = '';
        $data = $request->request->all() ;
        if($data)
            if( $data['email']){
                $message = \Swift_Message::newInstance()
                    ->setSubject('Contacto Pagina Web')
                    ->setFrom('info@romadeveloper.com')
                    ->setTo('main@romadeveloper.com')
                    ->setBody(
                        $this->render('emails/contact1.html.twig', array(
                            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
                            'data' => $data,
                        )),'text/html')
                    ->setReplyTo($data['email']);

                $this->get('mailer')->send($message);
                $info = '1';
            }
        //Correo Formulario ---//
        return $this->render('contact/index.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
            'actual' => '/contact',
            'base' => $this->base(),
            'info' => $info,
        ));
    }


    /**
     * @Route("/{_locale}/portfolio/{id}", name="portfolio" ,defaults={"id" = "main", "_locale" = "en"}, requirements={"_locale": "en|es"})
     * @Route("/portfolio/{id}" ,defaults={"id" = "nn", "_locale" = "en"})
     */
    public function portfolioAction(Request $request,$id)
    {
        if($id == 'main'){
            return $this->render('portfolio/index.html.twig', array(
                'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
                'actual' => '/portfolio',
                'portfolio' => $this->portfolio(10),
                'categorys' => $this->categorys_portfolio(),
                'base' => $this->base(),
            ));
        }else{
            return $this->render('portfolio/item.html.twig', array(
                'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
                'actual' => '/portfolio/'.$id,
                'item' => $this->portfolio_item($id),
                'base' => $this->base(),
            ));
        }
    }


    /**
     * @Route("/{_locale}/lp/{lp}", name="lp" ,defaults={"_locale" = "en","lp" = "webpage"}, requirements={"_locale": "en|es"})
     * @Route("/lp/{lp}" ,defaults={"_locale" = "en","lp" = "webpage"})
     *
     */
    public function lpAction(Request $request ,$lp)
    {
        $info = '';$data = $request->request->all() ;
        if($data)
            if( $data['email']){
                $message = \Swift_Message::newInstance()
                    ->setSubject('Contacto Pagina Web - Landing Page')
                    ->setFrom('info@romadeveloper.com')
                    ->setTo('main@romadeveloper.com')
                    ->setBody(
                        $this->render('emails/contact1.html.twig', array(
                            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
                            'data' => $data,
                        )),'text/html')
                    ->setReplyTo($data['email']);

                $this->get('mailer')->send($message);
                $info = '1';
            }
        //Correo Formulario ---//
        return $this->render('landingpage/'.$lp.'.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
            'actual' => '/lp/'.$lp,
            'base' => $this->base(),
            'info' => $info,
        ));
    }
    /**
     * @Route("/{_locale}/blog/{page}", name="blog" ,defaults={"_locale" = "en","page" = "1"}, requirements={"_locale": "en|es"})
     * @Route("/blog/{page}" ,defaults={"_locale" = "en","page" = "1"})
     *
     */
    public function blogAction(Request $request,$page)
    {
        // replace this example code with whatever you need
        $limit = 5;
        $blog = $this->getDoctrine()->getManager()
            ->getRepository('AppBundle:Post')
            ->findBy(
                array(),
                array('dateIni' => 'DESC'),
                $limit,
                ($limit * ($page - 1))
            );

        $paginate['page']  = $page;
        $totalpost = $this->getDoctrine()->getManager()->createQuery("SELECT COUNT(a) FROM AppBundle:Post a")->getSingleScalarResult();
        $paginate['pages']  = ceil($totalpost/$limit);
        if($paginate['pages'] == 1)
            $paginate = false;
        return $this->render('blog/index.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
            'actual' => '/blog',
            'base' => $this->base(),
            'blog'=>$blog,
            'paginate' => $paginate,
        ));
    }
    /**
     * @Route("/{_locale}/post/{id}", name="post" ,defaults={"_locale" = "en","id" = ""}, requirements={"_locale": "en|es"})
     * @Route("/post/{id}" ,defaults={"_locale" = "en","id" = ""})
     *
     */
    public function postAction(Request $request,$id)
    {
        if($id){
            $post = $this->getDoctrine()->getManager()
                ->getRepository('AppBundle:Post')
                ->find($id);
            return $this->render('blog/post.html.twig', array(
                'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
                'actual' => '/blog',
                'base' => $this->base(),
                'post'=>$post,
            ));

        }
        return $this->redirectToRoute('blog');
    }

    /**
     * @Route("/{_locale}/blog/search/{type}/{parameter}", name="blog/search",defaults={"_locale" = "en","type" = "tag","parameter" = ""}, requirements={"_locale": "en|es"})
     * @Route("/blog/search//{type}/{parameter}" ,defaults={"_locale" = "en","type" = "tag","parameter" = ""})
     */
    public function searchBlogAction(Request $request,$type,$parameter )
    {
        $base = $this->base();
        $repo = $this->getDoctrine()
            ->getRepository('AppBundle:Post');
        switch($type){
            case 'tag':
                if($parameter){
//                    $query = $repo->createQueryBuilder('a')
//                        ->where('a.tags LIKE :tag')
//                        ->setParameter('tag', '%'.$parameter.'%')
//                        ->getQuery();
//                    $blog = $query->getResult();

                    $query = $repo->createQueryBuilder('b')
                        ->innerJoin('b.tags', 't')
                        ->where('t.name LIKE :tag')
                        ->setParameter('tag', '%'.$parameter.'%')
                        ->getQuery();
                    $blog = $query->getResult();

                }

                break;
            case 'title':
                if( $data = $request->request->all()){

                    $query = $repo->createQueryBuilder('a')
                        ->where('a.titleEn LIKE :title OR a.titleEs LIKE :title')
                        ->setParameter('title', '%'.$data['title'].'%')
                        ->getQuery();
                    $blog = $query->getResult();

                }else{
                    $blog = $this->getDoctrine()->getManager()
                        ->getRepository('AppBundle:Post')
                        ->findBy(
                            array(),
                            array('date' => 'DESC'),
                            10,
                            0
                        );
                }

                break;
            default:
                $blog = $this->getDoctrine()->getManager()
                    ->getRepository('AppBundle:Post')
                    ->findBy(
                        array(),
                        array('date' => 'DESC'),
                        10,
                        0
                    );
                break;
        }
        return $this->render('blog/index.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
            'location' => $request->getLocale(),
            'actual' => '/blog',
            'base' => $base,
            'paginate' => false,
            'blog' => $blog,
        ));
    }
}
