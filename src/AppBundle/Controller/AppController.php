<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Tag;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class AppController extends Controller
{
    protected  function appBase(){
        $todo=0;
        $base['username'] = $this->getUser()->getIdentity()->getName();
        $base['userid'] = $this->getUser()->getId();
        $dateStart =  new \DateTime("now");
//        $dateStart =  $dateStart->sub(new \DateInterval('P1D'));
        $dateStart = date_format($dateStart,'Y-m-d');
        $repo = $this->getDoctrine()->getRepository('AppBundle:Event');
        $query = $repo->createQueryBuilder('e')
            ->innerJoin('e.lead', 'l')
            ->innerJoin('l.from', 'f')
            ->where('f.idUser = :idUser AND e.dateStart LIKE :dateStart '  )
            ->setParameter('idUser', $base['userid'])
            ->setParameter('dateStart', $dateStart.'%' )
            ->getQuery();
        $events = $query->getResult();
        $base['todo']= count($events);
        $base['error']= false;
        $base['msg']= 'sd';
        $base['tags'] = false;
        return $base;
    }
    protected function getTags($json = false){
        $tags = $this->getDoctrine()->getManager()
            ->getRepository('AppBundle:Tag')
            ->findBy(
                array(),
                array('name' => 'DESC')
            );
        if($json){
            $array = array();
            foreach($tags as $tag){
                $array[$tag->getId()] = $tag->getTag();
            }
            $array = json_encode($array);
            $tags = $array;
        }

        return $tags;
    }
    /**
     * @Route("/{_locale}/app", name="app",defaults={"_locale" = "en"}, requirements={"_locale": "en|es"})
     * @Route("/app" ,defaults={"_locale" = "en"})
     */
    public function appAction(Request $request)
    {
        $appBase = $this->appBase();
        // replace this example code with whatever you need
        return $this->render('app/default/index.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
            'location' => $request->getLocale(),
            'actual' => '/app',
            'appbase' => $appBase,
        ));
    }
}
