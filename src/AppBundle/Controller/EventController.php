<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Identity;
use AppBundle\Entity\Tag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Event;
use Exception;

class EventController extends AppController
{
    private $actual = "/app/event";
    private function eventtype(){
        return $this->getDoctrine()->getManager()->getRepository('AppBundle:EventType')->findAll();
    }
    /**
     * Lists all Events entities.
     *
     * @Route("/{_locale}/app/event/{filter}", name="app/event",defaults={"_locale" = "en","filter" = ""}, requirements={"_locale": "en|es"})
     * @Route("/app/event/{filter}")
     *
     */
    public function EventAction($filter)
    {
        $repo = $this->getDoctrine()->getRepository('AppBundle:Event');
        switch($filter){
            case 'lead':
                $this->actual = "/app/event/lead";
                $query = $repo->createQueryBuilder('a')
                    ->where('a.idLead IS NOT NULL')
                    ->getQuery();
                $events = $query->getResult();

                break;
            case 'project':
                $query = $repo->createQueryBuilder('a')
                    ->where('a.idProject IS NOT NULL')
                    ->getQuery();
                $events = $query->getResult();
                break;
            case 'user':
                $events = $this->getDoctrine()->getManager()->getRepository('AppBundle:Event')->findBy(array('idProject' => NULL));
                break;
            default :
                $events = $this->getDoctrine()->getManager()->getRepository('AppBundle:Event')->findBy(array('idProject' => NULL));
                break;

        }
        return $this->render('app/event/index.html.twig', array(
            'appbase' => $this->appBase(),
            'actual' => $this->actual,
            'events' => $events,
            'eventtype' => $this->eventtype(),
        ));
    }


    /**
     * @Route("/{_locale}/app/search/event/{type}/{parameter}", name="app/search/event",defaults={"_locale" = "en","type" = "byLead","parameter" = ""}, requirements={"_locale": "en|es"})
     * @Route("/app/search/event/{type}/{parameter}" ,defaults={"_locale" = "en","type" = "tag","parameter" = ""})
     */
    public function searchEventAction($type,$parameter )
    {
        $this->actual = "/app/search/event";
        switch($type){
            case 'byLead':
                if($parameter){
                    $events = $this->getDoctrine()->getManager()->getRepository('AppBundle:Event')->findByIdLead($parameter);
                    $lead = $this->getDoctrine()->getManager()->getRepository('AppBundle:Lead')->find($parameter);
                }
                break;
            case 'byProject':
                if($parameter){
                    $events = $this->getDoctrine()->getManager()->getRepository('AppBundle:Event')->findByIdProject($parameter);
                }

                break;
            default:

                break;
        }


        return $this->render('app/event/index.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
            'appbase' => $this->appBase(),
            'actual' => $this->actual,
            'events' => $events,
            'lead' => $lead,
            'type' => $type,
            'eventtype' => $this->eventtype(),
        ));
    }

    /**
     * Lists all Lead entities.
     *
     * @Route("/app/event/ajax/new", name="app/event/ajax/new")
     *
     */
    public function EventAjaxNewAction(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(array('message' => 'You can access this only using Ajax!'), 400);
        }
        try {
            //$data['start']
            $event = new Event();
            $data = $request->request->all() ;
            $dataIni =  $dateChange =  new \DateTime("now");
            $dateStart = $dateEnd =  new \DateTime($data['start']);
            $idLead = $idProject = null;
            $idEventStatus = 1;
            if(isset($data['end']))
                $dateEnd =  new \DateTime($data['end']);
            $idEventType = $data['type'];
            $description =  $data['description'];
            if(isset($data['idlead'])){
                $idLead =  $data['idlead'];
                $event->setIdLead($idLead);
                $event->setLead($this->getDoctrine()->getManager()->getRepository('AppBundle:Lead')->find($idLead));
            }
            $event->setIdProject($idProject);
            $event->setDateIni($dataIni);
            $event->setDateStart($dateStart);
            $event->setDateChange($dateChange);
            $event->setDateEnd($dateEnd);
            $event->setDescription($description);
            $event->setIdEventType($idEventType);
            $event->setType($this->getDoctrine()->getManager()->getRepository('AppBundle:EventType')->find($idEventType));
            $event->setIdStatus($idEventStatus);
            $event->setEventStatus($this->getDoctrine()->getManager()->getRepository('AppBundle:EventStatus')->find($idEventStatus));
            $em = $this->getDoctrine()->getManager();
            $em->persist($event);
            $em->flush();
            return new JsonResponse(array('message' => 'Success!'.json_encode($dateStart).json_encode($dateEnd),"idEvent"=>$event->getId()), 200);

        } catch (Exception $e) {
            return new JsonResponse(array('message' => json_encode($e->getMessage() /*$dateEnd*/)), 400);
        }
    }
    /**
     * Lists all Lead entities.
     *
     * @Route("app/event/ajax/update/{type}", name="app/event/ajax/update",defaults={"type" = "date"})
     *
     */
    public function EventAjaxUpdateAction(Request $request,$type)
    {
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(array('message' => 'You can access this only using Ajax!'), 400);
        }
        switch($type){
            case'date':
                try {
                    $data = $request->request->all() ;
                    $event = $this->getDoctrine()->getManager()->getRepository('AppBundle:Event')->find($data['id']);
                    $dateChange =  new \DateTime("now");
                    $dateStart = $dateEnd =  new \DateTime($data['start']);
                    if(isset($data['end']))
                        $dateEnd =  new \DateTime($data['end']);
                    $event->setDateStart($dateStart);
                    $event->setDateChange($dateChange);
                    $event->setDateEnd($dateEnd);
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($event);
                    $em->flush();
                    return new JsonResponse(array('message' => 'Success!'.json_encode($data['start'])), 200);

                } catch (Exception $e) {
                    return new JsonResponse(array('message' => json_encode($e->getMessage() /*$dateEnd*/)), 400);
                }
                break;
            case'status':
                try {
                    $data = $request->request->all() ;
                    $event = $this->getDoctrine()->getManager()->getRepository('AppBundle:Event')->find($data['id']);
                    $idEventStatus = 1 ;
                    if($data['status']=='done'){
                        $idEventStatus = 2;
                    }
                    $event->setIdStatus($idEventStatus);
                    $event->setEventStatus($eventStatus = $this->getDoctrine()->getManager()->getRepository('AppBundle:EventStatus')->find($idEventStatus));
                    $event->setColor($eventStatus->getColor());
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($event);
                    $em->flush();
                    return new JsonResponse(array('message' => 'Success!','color'=>$eventStatus->getColor()), 200);

                } catch (Exception $e) {
                    return new JsonResponse(array('message' => json_encode($e->getMessage() /*$dateEnd*/)), 400);
                }
                break;

        }
    }

    /**
     * Lists all Lead entities.
     *
     * @Route("/app/event/ajax/lead", name="app/event/ajax/lead")
     *
     */
    public function EventAjaxLeadAction(Request $request){
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(array('message' => 'You can access this only using Ajax!'), 400);
        }
        try {
            $data = $request->request->all() ;
            $lead = $this->getDoctrine()->getManager()->getRepository('AppBundle:Lead')->find($data['id']);
            $datas = $lead->getIdentity()->getDatas();
            $name = $lead->getIdentity()->getName();
            $status = $this->getDoctrine()->getManager()->getRepository('AppBundle:Event')->find($data['idEvent']);

            $status = $status->getIdEventStatus();
            $json= array();
            foreach($datas as $d){
                $json[] = array("name"=>$d->getDataType()->getName(),"value"=> $d->getVal());
            }
            return new JsonResponse(array("name" =>$name,"data"=>$json,"status"=>$status), 200);

        } catch (Exception $e) {
            return new JsonResponse(array('message' => json_encode($e->getMessage() /*$dateEnd*/)), 400);
        }
    }
    /**
     * Delete Lead
     *
     * @Route("/app/event/ajax/delete", name="app/event/ajax/delete")
     *
     */
    public function EventAjaxDelete(Request $request){
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(array('message' => 'You can access this only using Ajax!'), 400);
        }
        try {
            $data = $request->request->all() ;
            $event = $this->getDoctrine()->getManager()->getRepository('AppBundle:Event')->find($data['id']);
            $em = $this->getDoctrine()->getManager();
            $em->remove($event);
            $em->flush();
            return new JsonResponse(array("id"=>$data['id']), 200);

        } catch (Exception $e) {
            return new JsonResponse(array('message' => json_encode($e->getMessage() /*$dateEnd*/)), 400);
        }
    }

}
