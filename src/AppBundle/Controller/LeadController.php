<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Event;
use AppBundle\Entity\Identity;
use AppBundle\Entity\IdentityData;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Lead;
use AppBundle\Form\LeadType;

class LeadController extends AppController
{
    private $actual = "/app/lead";
    /**
     * Lists all Lead entities.
     * @Route("/{_locale}/app/lead", name="app/lead",defaults={"_locale" = "en"}, requirements={"_locale": "en|es"})
     * @Route("/app/lead")
     */
    public function LeadAction()
    {

        $repo = $this->getDoctrine()->getRepository('AppBundle:Lead');
        $query = $repo->createQueryBuilder('l')
            ->innerJoin('l.identity', 'i')
            ->orderBy('i.dateIni', 'DESC')
            ->getQuery();
        $leads = $query->getResult();

        return $this->render('app/lead/index.html.twig', array(
            'leads' => $leads,
            'appbase' => $this->appBase(),
            'actual' => $this->actual,
        ));
    }
    /**
     * @Route("/{_locale}/app/search/lead/{type}/{parameter}", name="app/search/lead",defaults={"_locale" = "en","type" = "events","parameter" = ""}, requirements={"_locale": "en|es"})
     * @Route("/app/search/lead/{type}/{parameter}" ,defaults={"_locale" = "en","type" = "tag","parameter" = ""})
     */
    public function searchLeadAction(Request $request,$type,$parameter )
    {
        $repo = $this->getDoctrine()->getRepository('AppBundle:Event');
        switch($type){
            case 'events':
                if($parameter){
                    $query = $repo->createQueryBuilder('a')
                        ->where('a.idLead LIKE :idlead')
                        ->setParameter('idlead', '%'.$parameter.'%')
                        ->getQuery();
                    $events = $query->getResult();

                }

                break;
            case 'tittle':
                if( $data = $request->request->all()){

                    $query = $repo->createQueryBuilder('a')
                        ->where('a.titleEn LIKE :tittle OR a.titleEs LIKE :tittle')
                        ->setParameter('title', '%'.$data['tittle'].'%')
                        ->getQuery();
                    $blog = $query->getResult();

                }

                break;
            default:

                break;
        }

        return $this->render('app/lead/search.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
            'events' => $events,
            'appbase' => $this->appBase(),
            'actual' => $this->actual,
        ));
    }
    /**
     * Creates a new Lead entity.
     *
     * @Route("/app/new/lead", name="app/new/lead")
     */
    public function newLeadAction(Request $request)
    {
        $this->actual = "/app/new/lead";
        $data = $request->request->all() ;
        $categorys = $this->getDoctrine()->getManager()->getRepository('AppBundle:LeadCategory')->findAll();
        $froms = $this->getDoctrine()->getManager()->getRepository('AppBundle:LeadFrom')->findAll();
        $templates = $this->getDoctrine()->getManager()->getRepository('AppBundle:LeadTemplate')->findAll();
        if($data){
            try {
                $lead = new Lead();
                $dataIni =  $dateChange =  new \DateTime("now");
                $em = $this->getDoctrine()->getManager();
                $identity = new Identity();
                $identity->setName($data['lead']['identity']['name']);
                $identity->setDateIni($dataIni);
                $em->persist($identity);
                $em->flush();
                foreach($data['lead']['identity']['data'] as $iType => $iDatas){
                    foreach($iDatas as $iData){
                        $indetityData = new IdentityData();
                        $indetityData->setIdDataType($iType);
                        $dataType = $this->getDoctrine()->getManager()->getRepository('AppBundle:DataType')->find($iType);
                        if($dataType){
                            $indetityData->setDataType($dataType);
                        }
                        $indetityData->setIdentity($identity);
                        $indetityData->setVal($iData);
                        $indetityData->setMain(false);
                        $em->persist($indetityData);
                        $identity->addData($indetityData);
                    }
                }
                $lead->setIdentity($identity);

                $lead->setIdFrom($data['lead']['idFrom']);
                $lead->setFrom($this->getDoctrine()->getManager()->getRepository('AppBundle:LeadFrom')->find($data['lead']['idFrom']));
                $lead->setIdMainCategory($data['lead']['idCategory']);
//                $lead->setMainCategory($this->getDoctrine()->getManager()->getRepository('AppBundle:LeadFrom')->find($data['lead']['idCategoty']));

                $lead->setDescription('Falta Descrip');
                if($data['events']){
                    foreach($data['events'] as $eventData){
                        $event = new Event();
                        $dateStart = $dateEnd =  new \DateTime($eventData['dateStart']);
                        if(isset($data['end']))
                            $dateEnd =  new \DateTime($data['end']);
                        $idEventType = $eventData['type'];
                        $description =  $eventData['description'];
                        $event->setDateIni($dataIni);
                        $event->setDateStart($dateStart);
                        $event->setDateChange($dateChange);
                        $event->setDateEnd($dateEnd);
                        $event->setDescription($description);
                        $event->setIdEventType($idEventType);
                        $event->setType($this->getDoctrine()->getManager()->getRepository('AppBundle:EventType')->find($idEventType));
                        $event->setIdStatus(1);
                        $event->setEventStatus($this->getDoctrine()->getManager()->getRepository('AppBundle:EventStatus')->find(1));
                        $event->setLead($lead);
                        $em->persist($event);
                        $lead->addEvent($event);
                    }
                }
                $em->persist($lead);
                $em->flush();

                $this->addFlash('info', 'The Lead was created successfully.');
            } catch (Exception $e) {

                $this->addFlash('error', 'Error.');
            }

            return $this->redirectToRoute('app/lead');
        }

        return $this->render('app/lead/new.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
            'appbase' => $this->appBase(),
            'actual' =>  $this->actual,
            'categorys'=> $categorys,
            'froms'=> $froms,
            'templates' => $templates,
        ));
    }



    /**
     * Displays a form to edit an existing Lead entity. and do the action of edit
     *
     * @Route("/{_locale}/app/edit/lead/{id}", name="app/edit/lead",defaults={"_locale" = "en","id" = ""}, requirements={"_locale": "en|es"})
     * @Route("/app/edit/lead/{id}")
     *
     */
    public function editLeadAction($id,Request $request)
    {
        if($id){
            $lead = $this->getDoctrine()->getManager()->getRepository('AppBundle:Lead')->find($id);
            $datas = $lead->getIdentity()->getDatas();
            $data = $request->request->all() ;
            if($data){
                $em = $this->getDoctrine()->getManager();
                $identity = $lead->getIdentity();
                $identity->setName($data['lead']['identity']['name']);
                $em->persist($identity);
                foreach($data['lead']['identity']['data'] as $iType => $iDatas){
                    foreach($iDatas as $idIData => $iData){
                        $indetityData = $this->getDoctrine()->getManager()->getRepository('AppBundle:IdentityData')->find($idIData);

                        $indetityData->setVal($iData);
                        $indetityData->setMain(false);
                        $em->persist($indetityData);
                    }
                }
                $lead->setIdentity($identity);
                $em->persist($lead);
                $em->flush();
                $lead->setIdFrom($data['lead']['idFrom']);
                $lead->setFrom($this->getDoctrine()->getManager()->getRepository('AppBundle:LeadFrom')->find($data['lead']['idFrom']));
                $lead->setIdMainCategory($data['lead']['idCategory']);
//                $lead->setMainCategory($this->getDoctrine()->getManager()->getRepository('AppBundle:LeadFrom')->find($data['lead']['idCategoty']));
                $this->addFlash('info', 'The Lead was Edited successfully.');
                return $this->redirectToRoute('app/lead');

            }
            $categorys = $this->getDoctrine()->getManager()->getRepository('AppBundle:LeadCategory')->findAll();
            $froms = $this->getDoctrine()->getManager()->getRepository('AppBundle:LeadFrom')->findAll();
            return $this->render('app/lead/edit.html.twig', array(
                'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
                'appbase' => $this->appBase(),
                'lead' => $lead,
                'datas' =>$datas,
                'actual' =>  $this->actual,
                'categorys'=> $categorys,
                'froms'=> $froms,
            ));
        }

        return $this->redirectToRoute('app/lead');
    }

    /**
     * Deletes a Lead entity.
     *
     * @Route("/{_locale}/app/delete/event/{id}", name="app/delete/lead",defaults={"_locale" = "en","id" = ""}, requirements={"_locale": "en|es"})
     * @Route("/app/delete/event/{id}")
     *
     */
    public function deleteLeadAction($id)
    {
        if($id){
            try {
                $lead = $this->getDoctrine()->getManager()->getRepository('AppBundle:Lead')->find($id);
                $em = $this->getDoctrine()->getManager();
                $em->remove($lead);
                $em->flush();
                $this->addFlash('info', 'The Lead was Delete successfully.');

            } catch (Exception $e) {
                $this->addFlash('error', 'Error.');
            }
        }

        return $this->redirectToRoute('app/lead');
    }

}
