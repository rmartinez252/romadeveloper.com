<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Post;
use AppBundle\Entity\Tag;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class BlogController extends AppController
{

    /**
     * @Route("/{_locale}/app/blog", name="app/blog",defaults={"_locale" = "en"}, requirements={"_locale": "en|es"})
     * @Route("/app/blog" ,defaults={"_locale" = "en"})
     */
    public function blogAction(Request $request )
    {
        $appBase = $this->appBase();
        $appBase['tags'] = $this->getTags();
        $limit = 10;
        $blog = $this->getDoctrine()->getManager()
            ->getRepository('AppBundle:Post')
            ->findBy(
                array(),
                array('dateIni' => 'DESC'),
                $limit,
                0
            );
        return $this->render('app/blog/index.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
            'location' => $request->getLocale(),
            'actual' => '/app/blog',
            'appbase' => $appBase,
            'blog' => $blog,
        ));
    }
    /**
     * @Route("/{_locale}/app/search/blog/{type}/{parameter}", name="app/search/blog",defaults={"_locale" = "en","type" = "tag","parameter" = ""}, requirements={"_locale": "en|es"})
     * @Route("/app/search/blog/{type}/{parameter}" ,defaults={"_locale" = "en","type" = "tag","parameter" = ""})
     */
    public function searchBlogAction(Request $request,$type,$parameter )
    {
        $appBase = $this->appBase();
        $appBase['tags'] = $this->getTags();
        $limit = 10;
        $where = array();
        $repo = $this->getDoctrine()->getRepository('AppBundle:Post');
        switch($type){
            case 'tag':
                if($parameter){
                    $query = $repo->createQueryBuilder('a')
                        ->join('a.tags', 'd')
                        ->where('d.id LIKE :tag')
                        ->setParameter('tag', '%'.$parameter.'%')
                        ->getQuery();
                    $blog = $query->getResult();

                }

                break;
            case 'tittle':
                if( $data = $request->request->all()){

                    $query = $repo->createQueryBuilder('a')
                        ->where('a.titleEn LIKE :tittle OR a.titleEs LIKE :tittle')
                        ->setParameter('title', '%'.$data['tittle'].'%')
                        ->getQuery();
                    $blog = $query->getResult();

                }

                break;
            default:
                $blog = $this->getDoctrine()->getManager()
                    ->getRepository('AppBundle:Post')
                    ->findBy(
                        $where,
                        array('date' => 'DESC'),
                        $limit,
                        0
                    );
                break;
        }


        return $this->render('app/blog/index.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
            'location' => $request->getLocale(),
            'actual' => '/app/blog',
            'appbase' => $appBase,
            'blog' => $blog,
        ));
    }
    /**
     * @Route("/{_locale}/app/new/blog", name="app/new/blog",defaults={"_locale" = "en"}, requirements={"_locale": "en|es"})
     * @Route("/app/new/blog" ,defaults={"_locale" = "en"})
     */
    public function newBlogAction(Request $request)
    {
        $appBase =$this->appBase();
        $appBase['tags'] = $this->getTags();
        $data = $request->request->all() ;
        if($data){
            $post =  new Post();
            $now = new \DateTime("now");
            $post->setDateIni( $now );
            $post->setDatePublish( $now );

            foreach($data as $att => $valor){
                $set = 'set'.ucfirst($att);
                if($set!='setImg_en' & $set!='setImg_es' ){
                    if($set == 'setTags'){
                        foreach($valor as $t){
                            $tag = $this->getDoctrine()->getManager()->getRepository('AppBundle:Tag')->find($t);
                            if($tag)
                                $post->addTag($tag);
                        }
                    }
                    else
                        $post->$set($valor);
                }
            }
            $validator = $this->get('validator');
            $errors = $validator->validate($post);
            if (count($errors) > 0) {
                foreach($errors as $error)
                    $this->addFlash('error', (string) $error );
                return $this->render('app/blog/new.html.twig', array(
                    'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
                    'location' => $request->getLocale(),
                    'actual' => '/app/blog',
                    'appbase' => $appBase,
                ));

            }

            $em = $this->getDoctrine()->getManager();
            // tells Doctrine you want to (eventually) save the Product (no queries yet)
            $em->persist($post);

            // actually executes the queries (i.e. the INSERT query)
            $em->flush();


            $fileName = "main.jpg";

            if($request->files){
                $destino = "img/blog/".$post->getId();
                if(!is_dir($destino))
                    mkdir($destino);
                if(!is_dir($destino.'/es'))
                    mkdir($destino.'/es');
                if(!is_dir($destino.'/en'))
                    mkdir($destino.'/en');
                if($request->files->get('img_en')){
                    $file = $request->files->get('img_en')->move($destino.'/en', $fileName);
                    copy($destino.'/en/'.$fileName, $destino.'/es/'.$fileName);
                }
                if($request->files->get('img_es'))
                    $file = $request->files->get('img_es')->move($destino.'/es', $fileName);

            }
            $this->addFlash('info', 'The post was created successfully.');
            return $this->redirectToRoute('app/blog');
        }

        return $this->render('app/blog/new.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
            'location' => $request->getLocale(),
            'actual' => '/app/blog',
            'appbase' => $appBase,
        ));
    }


    /**
     * @Route("/{_locale}/app/view/blog/{id}", name="app/view/blog",defaults={"_locale" = "en","id"=""}, requirements={"_locale": "en|es"})
     * @Route("/app/view/blog/{id}" ,defaults={"_locale" = "en","id"=""})
     */
    public function viewBlogAction(Request $request,$id)
    {
        $appBase =$this->appBase();
        if($id){
            $post = $this->getDoctrine()->getManager()
                ->getRepository('AppBundle:Post')
                ->find($id);

            return $this->render('app/blog/view.html.twig', array(
                'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
                'location' => $request->getLocale(),
                'actual' => '/app/blog',
                'appbase' => $appBase,
                'post' => $post,
            ));
        }
        return $this->redirectToRoute('app/blog');
    }
    /**
     * @Route("/{_locale}/app/edit/blog/{id}", name="app/edit/blog",defaults={"_locale" = "en","id"=""}, requirements={"_locale": "en|es"})
     * @Route("/app/edit/blog/{id}" ,defaults={"_locale" = "en","id"=""})
     */
    public function editBlogAction(Request $request,$id)
    {
        $appBase =$this->appBase();
        $appBase['tags'] = $this->getTags();
        if($id){
            $post = $this->getDoctrine()->getManager()
                ->getRepository('AppBundle:Post')
                ->find($id);
            $data = $request->request->all() ;
            if($data){
                foreach($data as $att => $valor){
                    $set = 'set'.ucfirst($att);
                    if($set!='setImg_en' & $set!='setImg_es' ){
                        if($set == 'setTags'){
                            foreach($post->getTags() as $tag){
                                $post->removeTag($tag);
                            }
                            foreach($valor as $t){
                                $tag = $this->getDoctrine()->getManager()->getRepository('AppBundle:Tag')->find($t);
                                if($tag)
                                    $post->addTag($tag);
                            }

                        }
                        else
                            $post->$set($valor);
                    }
                }
                $validator = $this->get('validator');
                $errors = $validator->validate($post);
                if (count($errors) > 0) {
                    foreach($errors as $error)
                        $this->addFlash('error', (string) $error );
                    return $this->render('app/blog/edit.html.twig', array(
                        'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
                        'location' => $request->getLocale(),
                        'actual' => '/app/blog',
                        'appbase' => $appBase,
                    ));

                }

                $em = $this->getDoctrine()->getManager();
                // tells Doctrine you want to (eventually) save the Product (no queries yet)
                $em->persist($post);

                // actually executes the queries (i.e. the INSERT query)
                $em->flush();
                if($request->files){
                    $fileName = "main.jpg";
                    $destino = "img/blog/".$post->getId();
                    if($request->files->get('img_en')){
                        $file = $request->files->get('img_en')->move($destino.'/en', $fileName);
                    }
                    if($request->files->get('img_es'))
                        $file = $request->files->get('img_es')->move($destino.'/es', $fileName);
                }
                $this->addFlash('info', 'The item was edited successfully.');
                return $this->redirectToRoute('app/view/blog', array('id' => $id), 301);
            }
            return $this->render('app/blog/edit.html.twig', array(
                'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
                'location' => $request->getLocale(),
                'actual' => '/app/blog',
                'appbase' => $appBase,
                'post' => $post,
            ));
        }
        return $this->redirectToRoute('app/blog');
    }
    /**
     * @Route("/{_locale}/app/delete/blog/{id}/{confirm}", name="app/delete/blog",defaults={"_locale" = "en","id" = "","confirm" = ""}, requirements={"_locale": "en|es"})
     * @Route("/app/delete/blog/{id}/{confirm}" ,defaults={"_locale" = "en","id" = "","confirm" = ""})
     */
    public function deleteBlogAction(Request $request,$id,$confirm)
    {
        $appBase =$this->appBase();
        if($id){
            $post = $this->getDoctrine()->getManager()
                ->getRepository('AppBundle:Blog')
                ->find($id);
            if( $confirm == "confirm"){
                $em = $this->getDoctrine()->getManager();
                $em->remove($post);
                $em->flush();
                $destino = "img/blog/".$id;
                if(is_file($destino."/en/main.jpg"))
                    unlink($destino."/en/main.jpg");
                if(is_file($destino."/es/main.jpg"))
                    unlink($destino."/es/main.jpg");
                rmdir($destino."/es");
                rmdir($destino."/en");
                rmdir($destino);
                $this->addFlash('info', 'The item was delete successfully.');
                return $this->redirectToRoute('app/blog');
            }


            $this->addFlash('warning', "You're about to delete the item.");
            return $this->render('app/blog/delete.html.twig', array(
                'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
                'location' => $request->getLocale(),
                'actual' => '/app/blog',
                'appbase' => $appBase,
                'post' => $post,
            ));
        }
        return $this->redirectToRoute('app/blog');
    }

    /**
     * @Route("/{_locale}/app/new/tag", name="app/new/tag",defaults={"_locale" = "en"}, requirements={"_locale": "en|es"})
     * @Route("/app/new/tag" ,defaults={"_locale" = "en"})
     */
    public function newTagAction(Request $request)
    {
        $data = $request->request->all() ;
        if($data){
            $tag =  new Tag();
            $tag->setName($data['name']);
            $validator = $this->get('validator');
            $errors = $validator->validate($tag);
            if (count($errors) > 0) {
                foreach($errors as $error)
                    $this->addFlash('error', (string) $error );
                return $this->redirectToRoute('app/blog');
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($tag);
            $em->flush();
            $this->addFlash('info', 'The tag was created successfully.');
            return $this->redirectToRoute('app/blog');
        }
        $this->addFlash('warning', "The tag don't submit correctly.");
        return $this->redirectToRoute('app/blog');
    }
}
