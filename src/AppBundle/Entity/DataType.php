<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * dataType
 *
 * @ORM\Table(name="data_type")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\dataTypeRepository")
 */
class DataType
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @ORM\OneToMany(targetEntity="IdentityData", mappedBy="dataType")
     */
    private $identityDatas;


    public function __construct() {
        $this->identityDatas = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return dataType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return dataType
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Add identityData
     *
     * @param \AppBundle\Entity\IdentityData $identityData
     *
     * @return DataType
     */
    public function addIdentityData(\AppBundle\Entity\IdentityData $identityData)
    {
        $this->identityDatas[] = $identityData;

        return $this;
    }

    /**
     * Remove identityData
     *
     * @param \AppBundle\Entity\IdentityData $identityData
     */
    public function removeIdentityData(\AppBundle\Entity\IdentityData $identityData)
    {
        $this->identityDatas->removeElement($identityData);
    }

    /**
     * Get identityDatas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIdentityDatas()
    {
        return $this->identityDatas;
    }
}
