<?php

namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * TemplateEvent
 * @ORM\Table(name="template_event")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TemplateEventRepository")
 */
class TemplateEvent
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var int
     *
     * @ORM\Column(name="dateStart", type="integer")
     */
    private $dateStart;
    /**
     * @var int
     *
     * @ORM\Column(name="dateEnd", type="integer", nullable=true)
     */
    private $dateEnd;

    /**
     * @var string
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @var int
     *
     * @ORM\Column(name="idEventType", type="integer")
     */
    private $idEventType;

    /**
     * @ORM\ManyToOne(targetEntity="EventType", inversedBy="templateEvents")
     * @ORM\JoinColumn(name="idEventType", referencedColumnName="id")
     */
    private $type;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * Set dateStart
     *
     * @param integer $dateStart
     *
     * @return TemplateEvent
     */
    public function setDateStart($dateStart)
    {
        $this->dateStart = $dateStart;

        return $this;
    }
    /**
     * Get dateStart
     *
     * @return integer
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * Set dateEnd
     *
     * @param integer $dateEnd
     *
     * @return TemplateEvent
     */
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * Get dateEnd
     *
     * @return integer
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return TemplateEvent
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set idEventType
     *
     * @param integer $idEventType
     *
     * @return TemplateEvent
     */
    public function setIdEventType($idEventType)
    {
        $this->idEventType = $idEventType;

        return $this;
    }

    /**
     * Get idEventType
     *
     * @return integer
     */
    public function getIdEventType()
    {
        return $this->idEventType;
    }

    /**
     * Set type
     *
     * @param \AppBundle\Entity\EventType $type
     *
     * @return Event
     */
    public function setType(\AppBundle\Entity\EventType $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \AppBundle\Entity\EventType
     */
    public function getType()
    {
        return $this->type;
    }
}

