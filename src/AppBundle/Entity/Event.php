<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Event
 *
 * @ORM\Table(name="event")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EventRepository")
 */
class Event
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="idProject", type="integer", nullable=true)
     */
    private $idProject;
    /**
     * @var int
     *
     * @ORM\Column(name="idLead", type="integer", nullable=true)
     */
    private $idLead;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateIni", type="datetime")
     */
    private $dateIni;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateStart", type="datetime")
     */
    private $dateStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateChange", type="datetime")
     */
    private $dateChange;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateEnd", type="datetime")
     */
    private $dateEnd;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var int
     *
     * @ORM\Column(name="idEventType", type="integer")
     */
    private $idEventType;
    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=255, nullable=true)
     */
    private $color;

    /**
     * @ORM\ManyToOne(targetEntity="EventType", inversedBy="events")
     * @ORM\JoinColumn(name="idEventType", referencedColumnName="id")
     */
    private $type;
    /**
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="events")
     * @ORM\JoinColumn(name="idProject", referencedColumnName="id")
     */
    private $project;
    /**
     * @ORM\ManyToOne(targetEntity="Lead", inversedBy="events",cascade={"persist"})
     * @ORM\JoinColumn(name="idLead", referencedColumnName="id")
     */
    private $lead;
    /**
     * @ORM\OneToMany(targetEntity="Permission", mappedBy="event")
     */
    private $permissions;


    /**
     * @var int
     *
     * @ORM\Column(name="idStatus", type="integer")
     */
    private $idEventStatus;
    /**
     * @ORM\ManyToOne(targetEntity="EventStatus", inversedBy="events")
     * @ORM\JoinColumn(name="idEventStatus", referencedColumnName="id")
     */
    private $status;

    public function __construct() {

    $this->permissions = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idProject
     *
     * @param integer $idProject
     *
     * @return Event
     */
    public function setIdProject($idProject)
    {
        $this->idProject = $idProject;

        return $this;
    }

    /**
     * Get idProject
     *
     * @return int
     */
    public function getIdProject()
    {
        return $this->idProject;
    }

    /**
     * Set dateIni
     *
     * @param \DateTime $dateIni
     *
     * @return Event
     */
    public function setDateIni($dateIni)
    {
        $this->dateIni = $dateIni;

        return $this;
    }

    /**
     * Get dateIni
     *
     * @return \DateTime
     */
    public function getDateIni()
    {
        return $this->dateIni;
    }

    /**
     * Set dateStart
     *
     * @param \DateTime $dateStart
     *
     * @return Event
     */
    public function setDateStart($dateStart)
    {
        $this->dateStart = $dateStart;

        return $this;
    }

    /**
     * Get dateStart
     *
     * @return \DateTime
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * Set dateChange
     *
     * @param \DateTime $dateChange
     *
     * @return Event
     */
    public function setDateChange($dateChange)
    {
        $this->dateChange = $dateChange;

        return $this;
    }

    /**
     * Get dateChange
     *
     * @return \DateTime
     */
    public function getDateChange()
    {
        return $this->dateChange;
    }

    /**
     * Set dateEnd
     *
     * @param \DateTime $dateEnd
     *
     * @return Event
     */
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * Get dateEnd
     *
     * @return \DateTime
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Event
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set idEventType
     *
     * @param integer $idEventType
     *
     * @return Event
     */
    public function setIdEventType($idEventType)
    {
        $this->idEventType = $idEventType;
        return $this;
    }

    /**
     * Get idEventType
     *
     * @return int
     */
    public function getIdEventType()
    {
        return $this->idEventType;
    }

    /**
     * Set idEventStatus
     *
     * @param integer $idEventStatus
     *
     * @return Event
     */
    public function setIdStatus($idEventStatus)
    {
        $this->idEventStatus = $idEventStatus;
        return $this;
    }

    /**
     * Get idStatus
     *
     * @return int
     */
    public function getIdEventStatus()
    {
        return $this->idEventStatus;
    }
    /**
     * Set color
     *
     * @param string $color
     *
     * @return EventType
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set type
     *
     * @param \AppBundle\Entity\EventType $type
     *
     * @return Event
     */
    public function setType(\AppBundle\Entity\EventType $type = null)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * Get type
     *
     * @return \AppBundle\Entity\EventStatus
     */
    public function getType()
    {
        return $this->type;
    }
    /**
     * Set type
     *
     * @param \AppBundle\Entity\EventStatus $status
     *
     * @return Event
     */
    public function setEventStatus(\AppBundle\Entity\EventStatus $status = null)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * Get type
     *
     * @return \AppBundle\Entity\EventStatus
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set project
     *
     * @param \AppBundle\Entity\Project $project
     *
     * @return Event
     */
    public function setProject(\AppBundle\Entity\Project $project = null)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return \AppBundle\Entity\Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Add permission
     *
     * @param \AppBundle\Entity\Permission $permission
     *
     * @return Event
     */
    public function addPermission(\AppBundle\Entity\Permission $permission)
    {
        $this->permissions[] = $permission;

        return $this;
    }

    /**
     * Remove permission
     *
     * @param \AppBundle\Entity\Permission $permission
     */
    public function removePermission(\AppBundle\Entity\Permission $permission)
    {
        $this->permissions->removeElement($permission);
    }

    /**
     * Get permissions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPermissions()
    {
        return $this->permissions;
    }

    /**
     * Set idLead
     *
     * @param integer $idLead
     *
     * @return Event
     */
    public function setIdLead($idLead)
    {
        $this->idLead = $idLead;

        return $this;
    }

    /**
     * Get idLead
     *
     * @return integer
     */
    public function getIdLead()
    {
        return $this->idLead;
    }

    /**
     * Set lead
     *
     * @param \AppBundle\Entity\Lead $lead
     *
     * @return Event
     */
    public function setLead(\AppBundle\Entity\Lead $lead = null)
    {
        $this->lead = $lead;

        return $this;
    }

    /**
     * Get lead
     *
     * @return \AppBundle\Entity\Lead
     */
    public function getLead()
    {
        return $this->lead;
    }
    public function getFullCalendar(){
        $array = array();
        $array['title'] = $this->getType()->getName();
        $array['start'] = $this->getDateStart();
        $array['end'] = $this->getDateEnd();
	    $array['description'] = $this->getDescription();
		$array['className'] = array("event", "bg-color-greenLight");
		$array['icon'] = 'fa-check';
        return json_encode($array);
    }
}
