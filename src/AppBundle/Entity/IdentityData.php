<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * IdentityData
 *
 * @ORM\Table(name="identity_data")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\IdentityDataRepository")
 */
class IdentityData
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="idIdentity", type="integer", nullable=true)
     */
    private $idIdentity;

    /**
     * @var int
     *
     * @ORM\Column(name="idDataType", type="integer")
     */
    private $idDataType;

    /**
     * @var string
     *
     * @ORM\Column(name="val", type="string", length=255)
     */
    private $val;

    /**
     * @var bool
     *
     * @ORM\Column(name="main", type="boolean")
     */
    private $main;

    /**
     * @ORM\ManyToOne(targetEntity="Identity", inversedBy="datas",cascade={"persist"})
     * @ORM\JoinColumn(name="idIdentity", referencedColumnName="id")
     */
    private $identity;
    /**
     * @ORM\ManyToOne(targetEntity="DataType", inversedBy="identityDatas")
     * @ORM\JoinColumn(name="idDataType", referencedColumnName="id")
     */
    private $dataType;
//
//    /**
//     * Get dataTypeName
//     *
//     * @return string
//     */
//    public function getDataTypeName()
//    {
//        return $this->dataType->getName();
//    }
//    /**
//     * Get dataTypeName
//     *
//     * @return array
//     */
//    public function getDataType()
//    {
//        return $this->dataType;
//    }
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idIdentity
     *
     * @param integer $idIdentity
     *
     * @return IdentityData
     */
    public function setIdIdentity($idIdentity)
    {
        $this->idIdentity = $idIdentity;

        return $this;
    }

    /**
     * Get idIdentity
     *
     * @return int
     */
    public function getIdIdentity()
    {
        return $this->idIdentity;
    }

    /**
     * Set idDataType
     *
     * @param integer $idDataType
     *
     * @return IdentityData
     */
    public function setIdDataType($idDataType)
    {
        $this->idDataType = $idDataType;

        return $this;
    }

    /**
     * Get idDataType
     *
     * @return int
     */
    public function getIdDataType()
    {
        return $this->idDataType;
    }

    /**
     * Set val
     *
     * @param string $val
     *
     * @return IdentityData
     */
    public function setVal($val)
    {
        $this->val = $val;

        return $this;
    }

    /**
     * Get val
     *
     * @return string
     */
    public function getVal()
    {
        return $this->val;
    }

    /**
     * Set main
     *
     * @param boolean $main
     *
     * @return IdentityData
     */
    public function setMain($main)
    {
        $this->main = $main;

        return $this;
    }

    /**
     * Get main
     *
     * @return bool
     */
    public function getMain()
    {
        return $this->main;
    }

    /**
     * Set identity
     *
     * @param \AppBundle\Entity\Identity $identity
     *
     * @return IdentityData
     */
    public function setIdentity(\AppBundle\Entity\Identity $identity = null)
    {
        $this->identity = $identity;

        return $this;
    }

    /**
     * Get identity
     *
     * @return \AppBundle\Entity\Identity
     */
    public function getIdentity()
    {
        return $this->identity;
    }

    /**
     * Set dataType
     *
     * @param \AppBundle\Entity\DataType $dataType
     *
     * @return IdentityData
     */
    public function setDataType(\AppBundle\Entity\DataType $dataType = null)
    {
        $this->dataType = $dataType;

        return $this;
    }

    /**
     * Get dataType
     *
     * @return \AppBundle\Entity\DataType
     */
    public function getDataType()
    {
        return $this->dataType;
    }
}
