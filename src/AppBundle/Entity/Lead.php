<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Lead
 *
 * @ORM\Table(name="lead")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\LeadRepository")
 */
class Lead
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="idIdentity", type="integer")
     */
    private $idIdentity;

    /**
     * @var int
     *
     * @ORM\Column(name="idFrom", type="integer")
     */
    private $idFrom;

    /**
     * @var int
     *
     * @ORM\Column(name="idMainCategory", type="integer", nullable=true )
     */
    private $idMainCategory;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;


    /**
     * @ORM\ManyToOne(targetEntity="LeadFrom", inversedBy="leads")
     * @ORM\JoinColumn(name="idFrom", referencedColumnName="id")
     */
    private $from;

    /**
     * @ORM\OneToOne(targetEntity="Identity",cascade={"persist"})
     * @ORM\JoinColumn(name="idIdentity", referencedColumnName="id")
     */
    private $identity;
    /**
     * @ORM\OneToMany(targetEntity="Event", mappedBy="lead",cascade={"persist"})
     */
    private $events;

    /**
     * @ORM\ManyToOne(targetEntity="LeadCategory", inversedBy="leads")
     * @ORM\JoinColumn(name="idMainCategory", referencedColumnName="id")
     */
    private $category;
    public function __construct() {
        $this->events = new \Doctrine\Common\Collections\ArrayCollection();
    }
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idIdentity
     *
     * @param integer $idIdentity
     *
     * @return Lead
     */
    public function setIdIdentity($idIdentity)
    {
        $this->idIdentity = $idIdentity;

        return $this;
    }

    /**
     * Get idIdentity
     *
     * @return int
     */
    public function getIdIdentity()
    {
        return $this->idIdentity;
    }

    /**
     * Set idFrom
     *
     * @param integer $idFrom
     *
     * @return Lead
     */
    public function setIdFrom($idFrom)
    {
        $this->idFrom = $idFrom;

        return $this;
    }

    /**
     * Get idFrom
     *
     * @return int
     */
    public function getIdFrom()
    {
        return $this->idFrom;
    }

    /**
     * Set idMainCategory
     *
     * @param integer $idMainCategory
     *
     * @return Lead
     */
    public function setIdMainCategory($idMainCategory)
    {
        $this->idMainCategory = $idMainCategory;

        return $this;
    }

    /**
     * Get idMainCategory
     *
     * @return int
     */
    public function getIdMainCategory()
    {
        return $this->idMainCategory;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Lead
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set from
     *
     * @param \AppBundle\Entity\LeadFrom $from
     *
     * @return Lead
     */
    public function setFrom(\AppBundle\Entity\LeadFrom $from = null)
    {
        $this->from = $from;

        return $this;
    }

    /**
     * Get from
     *
     * @return \AppBundle\Entity\LeadFrom
     */
    public function getFrom()
    {
        return $this->from;
    }
    /**
     * Set idenity
     *
     * @param \AppBundle\Entity\Identity $identity
     *
     * @return Lead
     */
    public function setIdentity(\AppBundle\Entity\Identity $identity = null)
    {
        $this->identity = $identity;

        return $this;
    }

    /**
     * Get identity
     *
     * @return \AppBundle\Entity\Identity
     */
    public function getIdentity()
    {
        return $this->identity;
    }
    /**
     * Add event
     *
     * @param \AppBundle\Entity\Event $event
     *
     * @return Project
     */
    public function addEvent(\AppBundle\Entity\Event $event)
    {
        $this->events[] = $event;

        return $this;
    }

    /**
     * Remove event
     *
     * @param \AppBundle\Entity\Event $event
     */
    public function removeEvent(\AppBundle\Entity\Event $event)
    {
        $this->events->removeElement($event);
    }

    /**
     * Get events
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEvents()
    {
        return $this->events;
    }
}