<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Comment
 *
 * @ORM\Table(name="comment")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CommentRepository")
 */
class Comment
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text")
     */
    private $comment;

    /**
     * @var int
     *
     * @ORM\Column(name="idPost", type="integer")
     */
    private $idPost;
    /**
     * @var int
     *
     * @ORM\Column(name="idComment", type="integer", nullable=true)
     */
    private $idComment;

    /**
     * @ORM\ManyToOne(targetEntity="Post", inversedBy="comments")
     * @ORM\JoinColumn(name="idPost", referencedColumnName="id")
     */
    private $post;

    /**
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="commentParent")
     */
    private $subComment;

    /**
     * @ORM\ManyToOne(targetEntity="Comment", inversedBy="subCommet")
     * @ORM\JoinColumn(name="idComment", referencedColumnName="id")
     */
    private $commentParent;

    public function __construct() {
        $this->subComment = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Comment
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set idPost
     *
     * @param integer $idPost
     *
     * @return Comment
     */
    public function setIdPost($idPost)
    {
        $this->idPost = $idPost;

        return $this;
    }

    /**
     * Get idPost
     *
     * @return int
     */
    public function getIdPost()
    {
        return $this->idPost;
    }

    /**
     * Set idComment
     *
     * @param integer $idComment
     *
     * @return Comment
     */
    public function setIdComment($idComment)
    {
        $this->idComment = $idComment;

        return $this;
    }

    /**
     * Get idComment
     *
     * @return integer
     */
    public function getIdComment()
    {
        return $this->idComment;
    }

    /**
     * Set post
     *
     * @param \AppBundle\Entity\Post $post
     *
     * @return Comment
     */
    public function setPost(\AppBundle\Entity\Post $post = null)
    {
        $this->post = $post;

        return $this;
    }

    /**
     * Get post
     *
     * @return \AppBundle\Entity\Post
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * Add subComment
     *
     * @param \AppBundle\Entity\Comment $subComment
     *
     * @return Comment
     */
    public function addSubComment(\AppBundle\Entity\Comment $subComment)
    {
        $this->subComment[] = $subComment;

        return $this;
    }

    /**
     * Remove subComment
     *
     * @param \AppBundle\Entity\Comment $subComment
     */
    public function removeSubComment(\AppBundle\Entity\Comment $subComment)
    {
        $this->subComment->removeElement($subComment);
    }

    /**
     * Get subComment
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubComment()
    {
        return $this->subComment;
    }

    /**
     * Set commentParent
     *
     * @param \AppBundle\Entity\Comment $commentParent
     *
     * @return Comment
     */
    public function setCommentParent(\AppBundle\Entity\Comment $commentParent = null)
    {
        $this->commentParent = $commentParent;

        return $this;
    }

    /**
     * Get commentParent
     *
     * @return \AppBundle\Entity\Comment
     */
    public function getCommentParent()
    {
        return $this->commentParent;
    }
}
