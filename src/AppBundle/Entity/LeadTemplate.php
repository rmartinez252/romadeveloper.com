<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LeadTemplate
 *
 * @ORM\Table(name="lead_template")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\LeadTemplateRepository")
 */
class LeadTemplate
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @ORM\ManyToMany(targetEntity="TemplateEvent")
     * @ORM\JoinTable(name="lead_template_template_event",
     *      joinColumns={@ORM\JoinColumn(name="id_template", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="id_template_event", referencedColumnName="id", unique=true)}
     *      )
     */
    private $events;
    public function __construct() {
        $this->events = new \Doctrine\Common\Collections\ArrayCollection();

    }
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return LeadTemplate
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return LeadTemplate
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Add event
     *
     * @param \AppBundle\Entity\TemplateEvent $event
     *
     * @return LeadTemplate
     */
    public function addEvent(\AppBundle\Entity\TemplateEvent $event)
    {
        $this->events[] = $event;

        return $this;
    }

    /**
     * Remove event
     *
     * @param \AppBundle\Entity\TemplateEvent $event
     */
    public function removeEvent(\AppBundle\Entity\TemplateEvent $event)
    {
        $this->events->removeElement($event);
    }

    /**
     * Get events
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEvents()
    {
        return $this->events;
    }
}
