<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Client
 *
 * @ORM\Table(name="client")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ClientRepository")
 */
class Client
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="idIdentity", type="integer")
     */
    private $idIdentity;


    /**
     * @ORM\OneToOne(targetEntity="Identity")
     * @ORM\JoinColumn(name="idIdentity", referencedColumnName="id")
     */
    private $identity;
    /**
     * @ORM\ManyToMany(targetEntity="Identity")
     * @ORM\JoinTable(name="contact_person",
     *      joinColumns={@ORM\JoinColumn(name="id_client", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="id_identity", referencedColumnName="id", unique=true)}
     *      )
     */
    private $contactPersons;
    /**
     * @ORM\OneToMany(targetEntity="Project", mappedBy="client")
     */
    private $projects;

    public function __construct() {
        $this->contactPersons = new \Doctrine\Common\Collections\ArrayCollection();
        $this->projects = new \Doctrine\Common\Collections\ArrayCollection();
    }
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idIdentity
     *
     * @param integer $idIdentity
     *
     * @return Client
     */
    public function setIdIdentity($idIdentity)
    {
        $this->idIdentity = $idIdentity;

        return $this;
    }

    /**
     * Get idIdentity
     *
     * @return int
     */
    public function getIdIdentity()
    {
        return $this->idIdentity;
    }
    /**
     * Get Identity
     *
     * @return array
     */
    public function getIdentity()
    {
        return $this->identity;
    }

    /**
     * Set identity
     *
     * @param \AppBundle\Entity\Identity $identity
     *
     * @return Client
     */
    public function setIdentity(\AppBundle\Entity\Identity $identity = null)
    {
        $this->identity = $identity;

        return $this;
    }

    /**
     * Add contactPerson
     *
     * @param \AppBundle\Entity\Identity $contactPerson
     *
     * @return Client
     */
    public function addContactPerson(\AppBundle\Entity\Identity $contactPerson)
    {
        $this->contactPersons[] = $contactPerson;

        return $this;
    }

    /**
     * Remove contactPerson
     *
     * @param \AppBundle\Entity\Identity $contactPerson
     */
    public function removeContactPerson(\AppBundle\Entity\Identity $contactPerson)
    {
        $this->contactPersons->removeElement($contactPerson);
    }

    /**
     * Get contactPersons
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContactPersons()
    {
        return $this->contactPersons;
    }

    /**
     * Add project
     *
     * @param \AppBundle\Entity\Project $project
     *
     * @return Client
     */
    public function addProject(\AppBundle\Entity\Project $project)
    {
        $this->projects[] = $project;

        return $this;
    }

    /**
     * Remove project
     *
     * @param \AppBundle\Entity\Project $project
     */
    public function removeProject(\AppBundle\Entity\Project $project)
    {
        $this->projects->removeElement($project);
    }

    /**
     * Get projects
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProjects()
    {
        return $this->projects;
    }
}
