<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Identity
 *
 * @ORM\Table(name="identity")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\IdentityRepository")
 */
class Identity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateIni", type="date")
     */
    private $dateIni;

    /**
     * @ORM\OneToOne(targetEntity="User", mappedBy="identity")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="IdentityData", mappedBy="identity",cascade={"persist"})
     */

    private $datas;


    public function __construct() {
        $this->datas = new \Doctrine\Common\Collections\ArrayCollection();
    }
    /**
     * Get id
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDatas()
    {
        return $this->datas;
    }
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Identity
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set dateIni
     *
     * @param \DateTime $dateIni
     *
     * @return Identity
     */
    public function setDateIni($dateIni)
    {
        $this->dateIni = $dateIni;

        return $this;
    }

    /**
     * Get dateIni
     *
     * @return \DateTime
     */
    public function getDateIni()
    {
        return $this->dateIni;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Identity
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add data
     *
     * @param \AppBundle\Entity\IdentityData $data
     *
     * @return Identity
     */
    public function addData(\AppBundle\Entity\IdentityData $data)
    {
        $this->datas[] = $data;

        return $this;
    }

    /**
     * Remove data
     *
     * @param \AppBundle\Entity\IdentityData $data
     */
    public function removeData(\AppBundle\Entity\IdentityData $data)
    {
        $this->datas->removeElement($data);
    }
}
