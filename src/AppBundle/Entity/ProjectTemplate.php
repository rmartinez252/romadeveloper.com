<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProjectTemplate
 *
 * @ORM\Table(name="project_template")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProjectTemplateRepository")
 */
class ProjectTemplate
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity="ProjectType", mappedBy="mainTemplate")
     */
    private $projectTypes;

    /**
     * @ORM\ManyToMany(targetEntity="TemplateEvent")
     * @ORM\JoinTable(name="project_template_template_event",
     *      joinColumns={@ORM\JoinColumn(name="id_template", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="id_template_event", referencedColumnName="id", unique=true)}
     *      )
     */
    private $events;
    public function __construct() {
        $this->projectTypes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->events = new \Doctrine\Common\Collections\ArrayCollection();

    }
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ProjectTemplate
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return ProjectTemplate
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Add projectType
     *
     * @param \AppBundle\Entity\ProjectType $projectType
     *
     * @return ProjectTemplate
     */
    public function addProjectType(\AppBundle\Entity\ProjectType $projectType)
    {
        $this->projectTypes[] = $projectType;

        return $this;
    }

    /**
     * Remove projectType
     *
     * @param \AppBundle\Entity\ProjectType $projectType
     */
    public function removeProjectType(\AppBundle\Entity\ProjectType $projectType)
    {
        $this->projectTypes->removeElement($projectType);
    }

    /**
     * Get projectTypes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProjectTypes()
    {
        return $this->projectTypes;
    }

    /**
     * Add event
     *
     * @param \AppBundle\Entity\TemplateEvent $event
     *
     * @return ProjectTemplate
     */
    public function addEvent(\AppBundle\Entity\TemplateEvent $event)
    {
        $this->events[] = $event;

        return $this;
    }

    /**
     * Remove event
     *
     * @param \AppBundle\Entity\TemplateEvent $event
     */
    public function removeEvent(\AppBundle\Entity\TemplateEvent $event)
    {
        $this->events->removeElement($event);
    }

    /**
     * Get events
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEvents()
    {
        return $this->events;
    }
}
