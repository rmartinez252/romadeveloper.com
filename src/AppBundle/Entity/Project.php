<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Project
 *
 * @ORM\Table(name="project")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProjectRepository")
 */
class Project
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="idClient", type="integer")
     */
    private $idClient;

    /**
     * @var int
     *
     * @ORM\Column(name="idUser", type="integer")
     */
    private $idUser;

    /**
     * @var int
     *
     * @ORM\Column(name="idProjectType", type="integer")
     */
    private $idProjectType;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateIni", type="date")
     */
    private $dateIni;

    /**
     * @var int
     *
     * @ORM\Column(name="idPortfolio", type="integer", nullable=true)
     */
    private $idPortfolio;


    /**
     * @ORM\ManyToOne(targetEntity="Client", inversedBy="projects")
     * @ORM\JoinColumn(name="idClient", referencedColumnName="id")
     */
    private $client;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="projects")
     * @ORM\JoinColumn(name="idUser", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="ProjectType", inversedBy="projects")
     * @ORM\JoinColumn(name="idProjectType", referencedColumnName="id")
     */
    private $type;
    /**
     * @ORM\ManyToOne(targetEntity="Portfolio", inversedBy="projects")
     * @ORM\JoinColumn(name="idPortfolio", referencedColumnName="id")
     */
    private $portfolio;
    /**
     * @ORM\OneToMany(targetEntity="Event", mappedBy="project")
     */
    private $events;

    /**
     * @ORM\ManyToMany(targetEntity="Category", mappedBy="projects")
     */
    private $categorys;
    /**
     * @ORM\OneToMany(targetEntity="Permission", mappedBy="project")
     */
    private $permissions;

    public function __construct() {
        $this->events = new \Doctrine\Common\Collections\ArrayCollection();
        $this->categorys = new \Doctrine\Common\Collections\ArrayCollection();
        $this->permissions = new \Doctrine\Common\Collections\ArrayCollection();
    }
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idClient
     *
     * @param integer $idClient
     *
     * @return Project
     */
    public function setIdClient($idClient)
    {
        $this->idClient = $idClient;

        return $this;
    }

    /**
     * Get idClient
     *
     * @return int
     */
    public function getIdClient()
    {
        return $this->idClient;
    }

    /**
     * Set idUser
     *
     * @param integer $idUser
     *
     * @return Project
     */
    public function setIdUser($idUser)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return int
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * Set idProjectType
     *
     * @param integer $idProjectType
     *
     * @return Project
     */
    public function setIdProjectType($idProjectType)
    {
        $this->idProjectType = $idProjectType;

        return $this;
    }

    /**
     * Get idProjectType
     *
     * @return int
     */
    public function getIdProjectType()
    {
        return $this->idProjectType;
    }

    /**
     * Set dateIni
     *
     * @param \DateTime $dateIni
     *
     * @return Project
     */
    public function setDateIni($dateIni)
    {
        $this->dateIni = $dateIni;

        return $this;
    }

    /**
     * Get dateIni
     *
     * @return \DateTime
     */
    public function getDateIni()
    {
        return $this->dateIni;
    }

    /**
     * Set idPortfolio
     *
     * @param integer $idPortfolio
     *
     * @return Project
     */
    public function setIdPortfolio($idPortfolio)
    {
        $this->idPortfolio = $idPortfolio;

        return $this;
    }

    /**
     * Get idPortfolio
     *
     * @return int
     */
    public function getIdPortfolio()
    {
        return $this->idPortfolio;
    }

    /**
     * Set client
     *
     * @param \AppBundle\Entity\Client $client
     *
     * @return Project
     */
    public function setClient(\AppBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \AppBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Project
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set type
     *
     * @param \AppBundle\Entity\ProjectType $type
     *
     * @return Project
     */
    public function setType(\AppBundle\Entity\ProjectType $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \AppBundle\Entity\ProjectType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set portfolio
     *
     * @param \AppBundle\Entity\Portfolio $portfolio
     *
     * @return Project
     */
    public function setPortfolio(\AppBundle\Entity\Portfolio $portfolio = null)
    {
        $this->portfolio = $portfolio;

        return $this;
    }

    /**
     * Get portfolio
     *
     * @return \AppBundle\Entity\Portfolio
     */
    public function getPortfolio()
    {
        return $this->portfolio;
    }

    /**
     * Add event
     *
     * @param \AppBundle\Entity\Event $event
     *
     * @return Project
     */
    public function addEvent(\AppBundle\Entity\Event $event)
    {
        $this->events[] = $event;

        return $this;
    }

    /**
     * Remove event
     *
     * @param \AppBundle\Entity\Event $event
     */
    public function removeEvent(\AppBundle\Entity\Event $event)
    {
        $this->events->removeElement($event);
    }

    /**
     * Get events
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEvents()
    {
        return $this->events;
    }

    /**
     * Add category
     *
     * @param \AppBundle\Entity\Category $category
     *
     * @return Project
     */
    public function addCategory(\AppBundle\Entity\Category $category)
    {
        $this->categorys[] = $category;

        return $this;
    }

    /**
     * Remove category
     *
     * @param \AppBundle\Entity\Category $category
     */
    public function removeCategory(\AppBundle\Entity\Category $category)
    {
        $this->categorys->removeElement($category);
    }

    /**
     * Get categorys
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategorys()
    {
        return $this->categorys;
    }

    /**
     * Add permission
     *
     * @param \AppBundle\Entity\Permission $permission
     *
     * @return Project
     */
    public function addPermission(\AppBundle\Entity\Permission $permission)
    {
        $this->permissions[] = $permission;

        return $this;
    }

    /**
     * Remove permission
     *
     * @param \AppBundle\Entity\Permission $permission
     */
    public function removePermission(\AppBundle\Entity\Permission $permission)
    {
        $this->permissions->removeElement($permission);
    }

    /**
     * Get permissions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPermissions()
    {
        return $this->permissions;
    }
}
