<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Permission
 *
 * @ORM\Table(name="permission")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PermissionRepository")
 */
class Permission
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="idUser", type="integer")
     */
    private $idUser;

    /**
     * @var int
     *
     * @ORM\Column(name="idEvent", type="integer", nullable=true)
     */
    private $idEvent;

    /**
     * @var int
     *
     * @ORM\Column(name="idProject", type="integer", nullable=true)
     */
    private $idProject;

    /**
     * @var int
     *
     * @ORM\Column(name="idSystem", type="integer", nullable=true)
     */
    private $idSystem;

    /**
     * @var int
     *
     * @ORM\Column(name="permission", type="integer")
     */
    private $permission;

    /**
     * @ORM\ManyToMany(targetEntity="User", mappedBy="permission")
     */
    private $users;
    /**
     * @ORM\ManyToOne(targetEntity="Event", inversedBy="permission")
     * @ORM\JoinColumn(name="idEvent", referencedColumnName="id")
     */
    private $event;
    /**
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="permission")
     * @ORM\JoinColumn(name="idProject", referencedColumnName="id")
     */
    private $project;

    public function __construct() {
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idUser
     *
     * @param integer $idUser
     *
     * @return UserPermission
     */
    public function setIdUser($idUser)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return int
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * Set idEvent
     *
     * @param integer $idEvent
     *
     * @return UserPermission
     */
    public function setIdEvent($idEvent)
    {
        $this->idEvent = $idEvent;

        return $this;
    }

    /**
     * Get idEvent
     *
     * @return int
     */
    public function getIdEvent()
    {
        return $this->idEvent;
    }

    /**
     * Set idProject
     *
     * @param integer $idProject
     *
     * @return UserPermission
     */
    public function setIdProject($idProject)
    {
        $this->idProject = $idProject;

        return $this;
    }

    /**
     * Get idProject
     *
     * @return int
     */
    public function getIdProject()
    {
        return $this->idProject;
    }

    /**
     * Set idSystem
     *
     * @param integer $idSystem
     *
     * @return UserPermission
     */
    public function setIdSystem($idSystem)
    {
        $this->idSystem = $idSystem;

        return $this;
    }

    /**
     * Get idSystem
     *
     * @return int
     */
    public function getIdSystem()
    {
        return $this->idSystem;
    }

    /**
     * Set permission
     *
     * @param integer $permission
     *
     * @return UserPermission
     */
    public function setPermission($permission)
    {
        $this->permission = $permission;

        return $this;
    }

    /**
     * Get permission
     *
     * @return int
     */
    public function getPermission()
    {
        return $this->permission;
    }

    /**
     * Add user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Permission
     */
    public function addUser(\AppBundle\Entity\User $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \AppBundle\Entity\User $user
     */
    public function removeUser(\AppBundle\Entity\User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Set event
     *
     * @param \AppBundle\Entity\Event $event
     *
     * @return Permission
     */
    public function setEvent(\AppBundle\Entity\Event $event = null)
    {
        $this->event = $event;

        return $this;
    }

    /**
     * Get event
     *
     * @return \AppBundle\Entity\Event
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * Set project
     *
     * @param \AppBundle\Entity\Project $project
     *
     * @return Permission
     */
    public function setProject(\AppBundle\Entity\Project $project = null)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return \AppBundle\Entity\Project
     */
    public function getProject()
    {
        return $this->project;
    }
}
