<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProjectType
 *
 * @ORM\Table(name="project_type")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProjectTypeRepository")
 */
class ProjectType
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="idMainTemplate", type="integer")
     */
    private $idMainTemplate;

    /**
     * @ORM\ManyToOne(targetEntity="ProjectTemplate", inversedBy="projectTypes")
     * @ORM\JoinColumn(name="idMainTemplate", referencedColumnName="id")
     */
    private $mainTemplate;
    /**
     * @ORM\OneToMany(targetEntity="Project", mappedBy="type")
     */
    private $projects;
    /**
     * @ORM\ManyToMany(targetEntity="ProjectTemplate")
     * @ORM\JoinTable(name="project_type_project_template",
     *      joinColumns={@ORM\JoinColumn(name="id_project_type", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="id_template", referencedColumnName="id", unique=true)}
     *      )
     */
    private $template;
    public function __construct() {
        $this->projects = new \Doctrine\Common\Collections\ArrayCollection();
        $this->template = new \Doctrine\Common\Collections\ArrayCollection();


    }
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ProjectType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set idMainTemplate
     *
     * @param integer $idMainTemplate
     *
     * @return ProjectType
     */
    public function setIdMainTemplate($idMainTemplate)
    {
        $this->idMainTemplate = $idMainTemplate;

        return $this;
    }

    /**
     * Get idMainTemplate
     *
     * @return int
     */
    public function getIdMainTemplate()
    {
        return $this->idMainTemplate;
    }

    /**
     * Set mainTemplate
     *
     * @param \AppBundle\Entity\ProjectTemplate $mainTemplate
     *
     * @return ProjectType
     */
    public function setMainTemplate(\AppBundle\Entity\ProjectTemplate $mainTemplate = null)
    {
        $this->mainTemplate = $mainTemplate;

        return $this;
    }

    /**
     * Get mainTemplate
     *
     * @return \AppBundle\Entity\ProjectTemplate
     */
    public function getMainTemplate()
    {
        return $this->mainTemplate;
    }

    /**
     * Add project
     *
     * @param \AppBundle\Entity\Project $project
     *
     * @return ProjectType
     */
    public function addProject(\AppBundle\Entity\Project $project)
    {
        $this->projects[] = $project;

        return $this;
    }

    /**
     * Remove project
     *
     * @param \AppBundle\Entity\Project $project
     */
    public function removeProject(\AppBundle\Entity\Project $project)
    {
        $this->projects->removeElement($project);
    }

    /**
     * Get projects
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProjects()
    {
        return $this->projects;
    }

    /**
     * Add template
     *
     * @param \AppBundle\Entity\ProjectTemplate $template
     *
     * @return ProjectType
     */
    public function addTemplate(\AppBundle\Entity\ProjectTemplate $template)
    {
        $this->template[] = $template;

        return $this;
    }

    /**
     * Remove template
     *
     * @param \AppBundle\Entity\ProjectTemplate $template
     */
    public function removeTemplate(\AppBundle\Entity\ProjectTemplate $template)
    {
        $this->template->removeElement($template);
    }

    /**
     * Get template
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTemplate()
    {
        return $this->template;
    }
}
