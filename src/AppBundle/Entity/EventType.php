<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EventType
 *
 * @ORM\Table(name="event_type")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EventTypeRepository")
 */
class EventType
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;
    /**
     * @var int
     *
     * @ORM\Column(name="idIcon", type="integer")
     */
    private $idIcon;
    /**
     * @var int
     *
     * @ORM\Column(name="idColor", type="integer")
     */
    private $idColor;

    /**
     * @ORM\OneToMany(targetEntity="Event", mappedBy="type")
     */
    private $events;
    /**
     * @ORM\OneToMany(targetEntity="TemplateEvent", mappedBy="type")
     */
    private $templateEvent;

    /**
     * @ORM\ManyToOne(targetEntity="ColorFullCalendar", inversedBy="eventtype")
     * @ORM\JoinColumn(name="idColor", referencedColumnName="id")
     */
    private $color;
    /**
     * @ORM\ManyToOne(targetEntity="IconFullCalendar", inversedBy="eventtype")
     * @ORM\JoinColumn(name="idIcon", referencedColumnName="id")
     */
    private $icon;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return EventType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return EventType
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * Set color
     *
     * @param \AppBundle\Entity\ColorFullCalendar $color
     *
     * @return EventType
     */
    public function setColor(\AppBundle\Entity\ColorFullCalendar $color = null)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return \AppBundle\Entity\ColorFullCalendar
     */
    public function getColor()
    {
        return $this->color;
    }
    /**
     * Set icon
     *
     * @param \AppBundle\Entity\IconFullCalendar $icon
     *
     * @return EventType
     */
    public function setIcon(\AppBundle\Entity\IconFullCalendar $icon = null)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get icon
     *
     * @return \AppBundle\Entity\IconFullCalendar
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->events = new \Doctrine\Common\Collections\ArrayCollection();
        $this->templateEvents = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add event
     *
     * @param \AppBundle\Entity\Event $event
     *
     * @return EventType
     */
    public function addEvent(\AppBundle\Entity\Event $event)
    {
        $this->events[] = $event;

        return $this;
    }

    /**
     * Remove event
     *
     * @param \AppBundle\Entity\Event $event
     */
    public function removeEvent(\AppBundle\Entity\Event $event)
    {
        $this->events->removeElement($event);
    }

    /**
     * Get events
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEvents()
    {
        return $this->events;
    }
    /**
     * Add templateEvent
     *
     * @param \AppBundle\Entity\TemplateEvent $templateEvent
     *
     * @return EventType
     */
    public function addTemplateEvent(\AppBundle\Entity\TemplateEvent $templateEvent)
    {
        $this->templateEvents[] = $templateEvent;

        return $this;
    }

    /**
     * Remove event
     *
     * @param \AppBundle\Entity\TemplateEvent $templateEvent
     */
    public function removeTemplateEvent(\AppBundle\Entity\TemplateEvent $templateEvent)
    {
        $this->events->removeElement($templateEvent);
    }

    /**
     * Get events
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTemplateEvents()
    {
        return $this->templateEvents;
    }
}
